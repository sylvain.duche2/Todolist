import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';
import 'package:todolist/model/task.dart';
import 'package:todolist/ui/widgets/business/widget_task.dart';
import 'package:todolist/ui/widgets/common/widget_text_field.dart';

import '../../../widget_tester_extension.dart';

void main() {
  group('WidgetTask', () {
    testWidgets('check WidgetTask \"task\" param', (WidgetTester tester) async {
      Task task = Task("new_task", idNote: "mocked_id", isChecked: true);
      await tester.pumpMaterialAppWidget(WidgetTask(task));
      expect(find.text("new_task"), findsOneWidget);
      expect(tester.widget<Checkbox>(find.byType(Checkbox)).value, true);

      task = Task("new_task_2", idNote: "mocked_id_2", isChecked: false);
      await tester.pumpMaterialAppWidget(WidgetTask(task));
      expect(find.text("new_task"), findsNothing);
      expect(tester.widget<Checkbox>(find.byType(Checkbox)).value, false);
    });

    testWidgets('check WidgetTask \"isEditable\" param', (WidgetTester tester) async {
      Task task = Task("new_task", idNote: "mocked_id");
      await tester.pumpMaterialAppWidget(WidgetTask(task));
      expect(find.byType(TextFormField), findsNothing);
      expect(find.byIcon(Icons.close), findsNothing);

      await tester.pumpMaterialAppWidget(WidgetTask(task, isEditable: true));
      expect(find.byType(TextFormField), findsOneWidget);
      expect(find.byIcon(Icons.close), findsOneWidget);
    });

    testWidgets('check WidgetTask \"isFocused\" param', (WidgetTester tester) async {
      Task task = Task("new_task", idNote: "mocked_id");
      await tester.pumpMaterialAppWidget(WidgetTask(task));
      expect(find.byType(WidgetTextField), findsNothing);

      await tester.pumpMaterialAppWidget(WidgetTask(task, isEditable: false, isFocused: true));
      expect(find.byType(WidgetTextField), findsNothing);

      Get.focusScope?.unfocus();
      await tester.pumpMaterialAppWidget(WidgetTask(task, isEditable: true, isFocused: true));
      expect(tester.state<WidgetTextFieldState>(find.byType(WidgetTextField)).focusNode.hasFocus, true);

      Get.focusScope?.unfocus();
      await tester.pumpMaterialAppWidget(WidgetTask(task, isEditable: true, isFocused: false));
      expect(tester.state<WidgetTextFieldState>(find.byType(WidgetTextField)).focusNode.hasFocus, false);
    });
  });
}
