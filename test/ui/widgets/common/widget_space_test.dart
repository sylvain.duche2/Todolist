import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:todolist/ui/widgets/common/widget_space.dart';
import 'package:todolist/utils/app_theme.dart';

void main() {
  group('WidgetSpace', () {
    testWidgets('check WidgetSpace contains SizedBox with default size = AppTheme.margin16', (WidgetTester tester) async {
      await tester.pumpWidget(WidgetSpace());

      expect(find.byType(SizedBox), findsOneWidget);
      expect(tester.widget<WidgetSpace>(find.byType(WidgetSpace)).size, AppTheme.margin16);
    });

    testWidgets('check WidgetSpace contains SizedBox with size = given size', (WidgetTester tester) async {
      await tester.pumpWidget(WidgetSpace(AppTheme.margin8));

      expect(tester.widget<WidgetSpace>(find.byType(WidgetSpace)).size, AppTheme.margin8);
    });
  });
}
