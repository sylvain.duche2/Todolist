import 'package:flutter_test/flutter_test.dart';
import 'package:todolist/ui/widgets/common/widget_text.dart';

import '../../../widget_tester_extension.dart';

void main() {
  group('WidgetText', () {
    testWidgets('check WidgetText displays given text', (WidgetTester tester) async {
      String givenText = "my_text";
      await tester.pumpMaterialAppWidget(WidgetText(givenText));

      expect(find.text(givenText), findsOneWidget);
    });
  });
}
