import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:todolist/ui/widgets/common/widget_text_field.dart';

import '../../../widget_tester_extension.dart';

void main() {
  group('WidgetTextField', () {
    testWidgets('check widget manage given params', (WidgetTester tester) async {
      String initialValue = "my_text";
      double fontSize = 24.0;
      await tester.pumpMaterialAppWidget(WidgetTextField(initialValue: initialValue, fontSize: fontSize));

      expect(tester.widget<TextFormField>(find.byType(TextFormField)).initialValue, initialValue);
      expect(tester.widget<TextField>(find.byType(TextField)).style?.fontSize, fontSize);
    });

    testWidgets('check widget default params are not previous given params', (WidgetTester tester) async {
      String initialValue = "my_text";
      double fontSize = 24.0;
      await tester.pumpMaterialAppWidget(WidgetTextField());

      expect(tester.widget<TextFormField>(find.byType(TextFormField)).initialValue != initialValue, true);
      expect(tester.widget<TextField>(find.byType(TextField)).style?.fontSize != fontSize, true);
    });
  });
}
