import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:todolist/model/task.dart';
import 'package:todolist/utils/date_manager.dart';
import 'package:todolist/utils/log.dart';

class TaskRepository {
  final CollectionReference collection = FirebaseFirestore.instance.collection('tasks');

  Stream<QuerySnapshot> getStream(String idNote) {
    return collection.where('idNote', isEqualTo: idNote).snapshots();
  }

  Future<bool> addTask(Task task) {
    String taskId = DateManager.createDatedId("task");
    task.id = taskId;
    return collection.doc(taskId).set(task.toJson()).then((_) {
      log('Task added');
      return true;
    }).catchError((error) {
      log('Task add failed: $error');
      return false;
    });
  }

  Future<void> updateTask(Task task) async {
    await collection.doc(task.id).update(task.toJson());
  }

  Future<bool> deleteTask(Task task) async {
    return collection.doc(task.id).delete().then((value) {
      log('Task deleted');
      return true;
    }).onError((error, stackTrace) {
      log('Task delete failed: $error');
      return false;
    });
  }
}
