import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:todolist/model/note.dart';
import 'package:todolist/utils/date_manager.dart';
import 'package:todolist/utils/log.dart';

class NoteRepository {
  final CollectionReference collection = FirebaseFirestore.instance.collection('notes');

  Stream<QuerySnapshot> getStream() {
    return collection.snapshots();
  }

  Future<bool> addNote(Note note) async {
    String noteId = DateManager.createDatedId("note");
    note.id = noteId;
    return collection.doc(noteId).set(note.toJson()).then((_) {
      log('Note added');
      return true;
    }).catchError((error) {
      log('Note add failed: $error');
      return false;
    });
  }

  void updateNote(Note note) async {
    await collection.doc(note.id).update(note.toJson());
  }

  void deleteNote(Note note) async {
    await collection.doc(note.id).delete();
  }
}
