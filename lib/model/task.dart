import 'package:cloud_firestore/cloud_firestore.dart';

class Task {
  String? id;
  String content;
  bool isChecked;
  String idNote;

  Task(this.content, {required this.idNote, this.isChecked = false, this.id});

  factory Task.fromSnapshot(DocumentSnapshot snapshot) {
    final newNote = Task.fromJson(snapshot.data() as Map<String, dynamic>);
    newNote.id = snapshot.reference.id;
    return newNote;
  }

  factory Task.fromJson(Map<String, dynamic> json) => _taskFromJson(json);

  Map<String, dynamic> toJson() => _taskToJson(this);

  @override
  String toString() => 'Task<id: $id, content: $content, isChecked: $isChecked, idNote: $idNote>';

  static Task _taskFromJson(Map<String, dynamic> json) {
    return Task(
      json['content'] as String,
      isChecked: json['isChecked'] as bool,
      idNote: json['idNote'] as String,
    );
  }

  static Map<String, dynamic> _taskToJson(Task task) => <String, dynamic>{
        'content': task.content,
        'isChecked': task.isChecked,
        'idNote': task.idNote,
      };
}
