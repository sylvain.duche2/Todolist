import 'package:cloud_firestore/cloud_firestore.dart';

class Note {
  String? id;
  String title;

  Note(this.title, {this.id});

  factory Note.empty() {
    return Note('');
  }

  factory Note.fromSnapshot(DocumentSnapshot snapshot) {
    final newNote = Note.fromJson(snapshot.data() as Map<String, dynamic>);
    newNote.id = snapshot.reference.id;
    return newNote;
  }

  factory Note.fromJson(Map<String, dynamic> json) => _noteFromJson(json);

  Map<String, dynamic> toJson() => _noteToJson(this);

  @override
  String toString() => 'Note<$title>';

  static Note _noteFromJson(Map<String, dynamic> json) {
    return Note(
      json['title'] as String,
    );
  }

  static Map<String, dynamic> _noteToJson(Note note) => <String, dynamic>{
        'title': note.title,
      };
}
