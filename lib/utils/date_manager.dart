import 'package:intl/intl.dart';

class DateManager {
  static String createDatedId(String prefix) {
    return "$prefix${DateFormat('_yyyyMMdd_').format(DateTime.now()) + DateTime.now().millisecondsSinceEpoch.toString()}";
  }
}
