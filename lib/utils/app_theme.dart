class AppTheme {
  static const double margin16 = 16.0;
  static const double margin8 = 8.0;
  static const double borderRadius = 8.0;
}
