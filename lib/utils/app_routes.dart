import 'package:get/get.dart';
import 'package:todolist/ui/bindings/notes_binding.dart';
import 'package:todolist/ui/screens/screen_note_detail.dart';
import 'package:todolist/ui/screens/screen_note_list.dart';

class AppRoutes {
  static const String screenNoteList = "/noteList";
  static const String screenNoteDetail = "/noteList/noteDetail";

  // Note about Bindings
  // You might be wondering if Bindings are an overkill.
  // They are! But let’s say you have 10 controllers to be injected as dependencies.
  // Declaring them in view may not look very neat.
  // In this case Bindings are the way to go.
  // https://medium.com/flutter-community/the-flutter-getx-ecosystem-dependency-injection-8e763d0ec6b9

  static final screens = [
    GetPage(
      name: screenNoteList,
      page: () => ScreenNoteList(),
      binding: NotesBinding(),
      transition: Transition.noTransition,
    ),
    GetPage(
      name: screenNoteDetail,
      page: () => ScreenNoteDetail(),
      binding: NotesBinding(),
      transition: Transition.noTransition,
    ),
  ];
}
