import 'package:get/get.dart';
import 'package:todolist/ui/controllers/main_controller.dart';

class NotesBinding implements Bindings {
  @override
  void dependencies() {
    Get.put<MainController>(MainController());
  }
}
