import 'package:flutter/material.dart';
import 'package:todolist/model/note.dart';
import 'package:todolist/ui/widgets/common/widget_text.dart';
import 'package:todolist/utils/app_theme.dart';

class WidgetNote extends StatelessWidget {
  final Note note;
  final VoidCallback onPressed;
  final VoidCallback onLongPressed;

  const WidgetNote(
    this.note, {
    required this.onPressed,
    required this.onLongPressed,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.all(Radius.circular(AppTheme.borderRadius)),
      child: InkWell(
        onTap: onPressed,
        onLongPress: onLongPressed,
        borderRadius: BorderRadius.all(Radius.circular(AppTheme.borderRadius)),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.pink.shade100,
            border: Border.all(color: Colors.grey, width: 2),
            borderRadius: BorderRadius.all(
              Radius.circular(AppTheme.borderRadius),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(16),
            child: WidgetText(note.title),
          ),
        ),
      ),
    );
  }
}
