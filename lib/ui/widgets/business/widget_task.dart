import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:todolist/model/task.dart';
import 'package:todolist/ui/controllers/main_controller.dart';
import 'package:todolist/ui/widgets/common/widget_text.dart';
import 'package:todolist/ui/widgets/common/widget_text_field.dart';

class WidgetTask extends StatefulWidget {
  final Task task;
  final bool isEditable;
  final bool isFocused;

  WidgetTask(this.task, {this.isEditable = false, this.isFocused = false, Key? key}) : super(key: key);

  @override
  _WidgetTaskState createState() => _WidgetTaskState();
}

class _WidgetTaskState extends State<WidgetTask> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.green.shade100,
      child: Row(
        children: [
          Checkbox(
              value: widget.task.isChecked,
              onChanged: (checked) {
                setState(() {
                  widget.task.isChecked = checked ?? false;
                });
                onTaskCheckedChanged(checked ?? false);
              }),
          widget.isEditable
              ? Flexible(
                  child: WidgetTextField(
                    initialValue: widget.task.content,
                    isFocused: widget.isFocused,
                    onTextChanged: (newText) => onTaskTextChanged(newText),
                  ),
                )
              : WidgetText(widget.task.content),
          widget.isEditable
              ? Material(
                  color: Colors.transparent,
                  child: InkWell(
                    onTap: () => onTaskDeleteClicked(),
                    customBorder: CircleBorder(),
                    splashColor: Colors.grey,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Icon(Icons.close),
                    ),
                  ),
                )
              : Container(),
        ],
      ),
    );
  }

  // region Methods

  void onTaskDeleteClicked() {
    Get.focusScope?.unfocus();
    var controllerMain = Get.find<MainController>();
    controllerMain.deleteTask(widget.task);
  }

  void onTaskCheckedChanged(bool isChecked) {
    widget.task.isChecked = isChecked;
    var controllerMain = Get.find<MainController>();
    controllerMain.updateTask(widget.task);
  }

  void onTaskTextChanged(String newText) {
    widget.task.content = newText;
    var controllerMain = Get.find<MainController>();
    controllerMain.updateTask(widget.task);
  }

// endregion
}
