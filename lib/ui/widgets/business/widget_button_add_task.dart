import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:todolist/i18n/strings.g.dart';
import 'package:todolist/ui/controllers/main_controller.dart';
import 'package:todolist/ui/widgets/common/widget_text.dart';

class WidgetButtonAddTask extends StatefulWidget {
  const WidgetButtonAddTask({Key? key}) : super(key: key);

  @override
  _WidgetButtonAddTaskState createState() => _WidgetButtonAddTaskState();
}

class _WidgetButtonAddTaskState extends State<WidgetButtonAddTask> {
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.yellow.shade200,
      child: InkWell(
        onTap: () => onClicked(),
        child: Container(
          padding: EdgeInsets.all(16),
          child: Row(
            children: [
              Icon(Icons.add),
              WidgetText(i18n.widgetButtonAddTask.buttonText),
            ],
          ),
        ),
      ),
    );
  }

  // region Methods

  void onClicked() {
    Get.focusScope?.unfocus();
    var controllerMain = Get.find<MainController>();
    controllerMain.addTask();
  }

// endregion
}
