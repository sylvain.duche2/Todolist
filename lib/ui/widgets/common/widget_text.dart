import 'package:flutter/material.dart';

class WidgetText extends StatelessWidget {
  final String text;
  const WidgetText(this.text, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(text);
  }
}
