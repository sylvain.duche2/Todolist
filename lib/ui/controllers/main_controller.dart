import 'package:get/get.dart';
import 'package:todolist/model/note.dart';
import 'package:todolist/model/task.dart';
import 'package:todolist/repository/note_repository.dart';
import 'package:todolist/repository/task_repository.dart';
import 'package:todolist/utils/app_routes.dart';
import 'package:todolist/utils/log.dart';

class MainController extends GetxController {
  final NoteRepository noteRepository = NoteRepository();
  final TaskRepository taskRepository = TaskRepository();
  final selectedNote = Note.empty().obs;
  final isTaskDeletion = false.obs;
  final isTaskAddition = false.obs;

  // region Notes

  void onNewNoteClicked() {
    log("onNewNoteClicked");
    Note newNote = Note.empty();

    noteRepository.addNote(newNote).then((isOk) {
      if (isOk) {
        selectedNote.value = newNote;
        Get.toNamed(AppRoutes.screenNoteDetail);
      } else {
        Get.snackbar("Erreur", "Add note erreur");
      }
    });
  }

  void onNoteSelected(Note note) {
    log("onNoteSelected");
    selectedNote.value = note;
    Get.toNamed(AppRoutes.screenNoteDetail);
  }

  void onNoteTitleChanged(String newTitle) {
    log("onNoteTitleChanged");
    selectedNote.value.title = newTitle;
    noteRepository.updateNote(selectedNote.value);
  }

  void onNoteDelete(Note note) {
    log("onNoteDelete");
    noteRepository.deleteNote(selectedNote.value);
  }

  // endregion

  // region Tasks

  void addTask() {
    isTaskAddition.value = true;
    Task newTask = Task('', idNote: selectedNote.value.id!);
    taskRepository.addTask(newTask).then((isOk) {
      if (!isOk) {
        Get.snackbar("Erreur", "Add note erreur");
        isTaskAddition.value = false;
      }
    });
  }

  void deleteTask(Task task) {
    isTaskDeletion.value = true;
    taskRepository.deleteTask(task).then((isOk) {
      if (!isOk) {
        Get.snackbar("Erreur", "Delete task erreur");
        isTaskDeletion.value = false;
      }
    });
  }

  void updateTask(Task task) {
    taskRepository.updateTask(task);
  }

// endregion
}
