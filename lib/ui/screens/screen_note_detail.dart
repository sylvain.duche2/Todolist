import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:todolist/i18n/strings.g.dart';
import 'package:todolist/model/task.dart';
import 'package:todolist/ui/controllers/main_controller.dart';
import 'package:todolist/ui/widgets/business/widget_button_add_task.dart';
import 'package:todolist/ui/widgets/business/widget_task.dart';
import 'package:todolist/ui/widgets/common/widget_space.dart';
import 'package:todolist/ui/widgets/common/widget_text.dart';
import 'package:todolist/ui/widgets/common/widget_text_field.dart';
import 'package:todolist/utils/log.dart';

class ScreenNoteDetail extends GetView<MainController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
        ),
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: Icon(Icons.arrow_back),
        ),
        title: WidgetText(i18n.screenNoteDetail.title),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              color: Colors.pink.shade100,
              child: Padding(
                padding: const EdgeInsets.only(left: 16, right: 16, top: 8, bottom: 8),
                child: WidgetTextField(
                  hintText: i18n.screenNoteDetail.hintTextForTitle,
                  initialValue: controller.selectedNote.value.title,
                  isFocused: controller.selectedNote.value.title.isEmpty,
                  fontSize: 25,
                  onTextChanged: (newTitle) => controller.onNoteTitleChanged(newTitle),
                ),
              ),
            ),
            StreamBuilder<QuerySnapshot>(
              stream: controller.taskRepository.getStream(controller.selectedNote.value.id!),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return LinearProgressIndicator();
                } else {
                  bool isTaskDeletion = controller.isTaskDeletion.value;
                  controller.isTaskDeletion.value = false;
                  bool isTaskAddition = controller.isTaskAddition.value;
                  controller.isTaskAddition.value = false;
                  log("isTaskDeletion: $isTaskDeletion isTaskAddition: $isTaskAddition");
                  return _buildList(context, snapshot.data?.docs ?? [], isFocusable: isTaskAddition);
                }
              },
            ),
            WidgetButtonAddTask(),
          ],
        ),
      ),
    );
  }

  Widget _buildList(BuildContext context, List<DocumentSnapshot> snapshot, {bool isFocusable = false}) {
    return ListView.separated(
      shrinkWrap: true,
      padding: EdgeInsets.all(snapshot.length > 0 ? 16 : 0),
      physics: NeverScrollableScrollPhysics(),
      separatorBuilder: (context, index) => WidgetSpace(16),
      itemCount: snapshot.length,
      itemBuilder: (context, index) => _buildListItem(context, snapshot[index], isFocusable == true && index == snapshot.length - 1),
    );
  }

  Widget _buildListItem(BuildContext context, DocumentSnapshot snapshot, bool isFocused) {
    final task = Task.fromSnapshot(snapshot);
    return WidgetTask(
      task,
      isEditable: true,
      isFocused: isFocused,
      key: ValueKey(task.id),
    );
  }
}
