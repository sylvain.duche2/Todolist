import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import 'package:todolist/i18n/strings.g.dart';
import 'package:todolist/model/note.dart';
import 'package:todolist/ui/controllers/main_controller.dart';
import 'package:todolist/ui/widgets/business/widget_note.dart';
import 'package:todolist/ui/widgets/common/widget_text.dart';

class ScreenNoteList extends GetView<MainController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
        ),
        title: WidgetText(i18n.screenNoteList.title),
      ),
      body: Column(
        children: [
          StreamBuilder<QuerySnapshot>(
              stream: controller.noteRepository.getStream(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) return LinearProgressIndicator();

                return _buildList(context, snapshot.data?.docs ?? []);
              }),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => controller.onNewNoteClicked(),
        tooltip: i18n.screenNoteList.addButtonTooltip,
        child: const Icon(Icons.add),
      ),
    );
  }

  Widget _buildList(BuildContext context, List<DocumentSnapshot> snapshot) {
    return StaggeredGridView.countBuilder(
      crossAxisCount: 2,
      itemCount: snapshot.length,
      itemBuilder: (context, index) => _buildListItem(context, snapshot[index]),
      staggeredTileBuilder: (int index) => new StaggeredTile.fit(1),
      mainAxisSpacing: 16,
      crossAxisSpacing: 16,
      physics: NeverScrollableScrollPhysics(),
      padding: const EdgeInsets.all(16),
      shrinkWrap: true,
    );
  }

  Widget _buildListItem(BuildContext context, DocumentSnapshot snapshot) {
    final note = Note.fromSnapshot(snapshot);
    return WidgetNote(
      note,
      onPressed: () => controller.onNoteSelected(note),
      onLongPressed: () => controller.onNoteDelete(note),
    );
  }
}
